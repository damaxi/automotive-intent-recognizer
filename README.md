# Coding Challenge - Embedded Intent Recognizer

## Description of algorithm which was used

There are several approaches how to solving this problem. Looking forward to this specific use case: the intention recognized embedded solution in automotive. It was decided to implement it using Naive Bayes Engine. The data rule trainer was not in scope of this task. But the task itself shows how the Bayes characteristic looks like and how they can be manipulated.

Naive Bayes is trying to classify which word should be assigned to which rule. So it calculates prior probability of each word in every intention. Such probability is called conditional probability P("word"|"rule name").
Which equals the probability of the word in all sentences divided by the probability of the word in this particular rule. What creates relation of how often the word can be met both in this rule and all other rules.
The intention smoothing was also used in engine by eliminating all meaningless words.
In addition it may happen that the word can be substituted by other word or there many other unknown words which discfalify the intention. In that case the unknown words shall degraded the whole intention score.

## Architectural Decisions

It was taken into account that other engines might be implemented for instance decision tree engine. More over we can extend application easly:

- to add other rules in runtime when UI interface change (it can bring performance benefits)

- to handle which ResultRule might be handled in case when for example the rules have exact or almost exast score

- to handle additional analysis on calling rule handler, for instance: we can peform morphorogical analysis and extract the city from GetWeatherCity intetion

- the other component can be plugged in, in addition to REPL Interpreter, for instance: synthesize voice to text and push recognized text to engine to trigger some action and responce

## Task Description
Build a small intent recognition command line tool. The input and output looks like this:
What is the weather like today? => Prints (Intent: Get Weather)
What is the weather like in Paris today? => Prints (Intent: Get Weather City)
Tell me an interesting fact. => Prints (Intent: Get Fact)
Note that simply implementing the 3 use cases above without the coding being extendable and scalable will not be
sufficient to pass the test. The code only needs to handle the 3 sentences above but it should be clear how it could
be extended to handle more use cases.
Requirements
Your code has to be buildable with cmake using C++ and the command: `mkdir build && cd build && cmake .. &&
make`
should use modern C++ (>= C++17)
should be pushed to a github repository that we can clone, e.g. https://github.com/
should have good commit messages
should have unit tests (Catch2, Google Test or any other framework)
Hints
You should make your solution stand out by implementing and documenting one of the following areas:
Handling different semantic variations of the input, i.e. completely generic handling of the input
Having extendable and well designed software architecture
Having proven highly scalable performance
