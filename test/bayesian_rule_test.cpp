#include <catch2/catch_test_macros.hpp>

#include "bayesian_rule.h"

namespace eir = embedded::intent::recognizer;

TEST_CASE("Analyze by two rules") {
  auto dummy_cbk = [](eir::PreprocessedOutput const& output){ return ""; };
  eir::BayesianRule get_weather = {"GetWeather",
                                   {{"weather", 0.5}, {"today", 0.4}},
                                   dummy_cbk};
  eir::BayesianRule get_weather_city = {"GetWeatherCity",
                                        {{"weather", 0.4}, {"today", 0.4},
                                         {"in", 0.6}}, dummy_cbk};
  eir::BayesianRules rules = {get_weather, get_weather_city};
  SECTION("Full sentence") {
    eir::PreprocessedOutput output = {"weather", "in", "paris", "today"};
    auto results = eir::analyze_preprocessed_output_by_rules(output, rules);
    REQUIRE(results.size() == 2);
    REQUIRE(results[0].name == "GetWeather");
    REQUIRE(results[1].name == "GetWeatherCity");
    REQUIRE(results[1].score > results[0].score);
  }

  SECTION("One word") {
    eir::PreprocessedOutput output = {"unknown"};
    auto results = eir::analyze_preprocessed_output_by_rules(output, rules);
    REQUIRE(results.size() == 2);
    REQUIRE(results[0].name == "GetWeather");
    REQUIRE(results[1].name == "GetWeatherCity");
    REQUIRE(results[0].score == 0.1);
    REQUIRE(results[0].score == results[1].score);
  }
}
