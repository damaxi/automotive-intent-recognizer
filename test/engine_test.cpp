#include <catch2/catch_test_macros.hpp>

#include "bayesian_engine.h"

namespace eir = embedded::intent::recognizer;

using namespace std::string_literals;

TEST_CASE("TestUndefinedIntention") {
  eir::BayesianEngine engine;
  SECTION("Two undefined words") {
    std::string input = "undefined intention"s;
    REQUIRE(engine.recognize_intent(input).has_value() == false);
  }

  SECTION("One undefined word") {
    std::string input = "undefined"s;
    REQUIRE(engine.recognize_intent(input).has_value() == false);
  }

  SECTION("Empty intetion") {
    std::string input = ""s;
    REQUIRE(engine.recognize_intent(input).has_value() == false);
  }
}

TEST_CASE("Test supported intentions") {
  eir::BayesianEngine engine;
  SECTION("Check weather intention") {
    std::string input = "What is the weather like today?"s;
    REQUIRE(engine.recognize_intent(input) == "Intent: Get Weather");
  }

  SECTION("Check weather city intention") {
    std::string input = "What is the weather like in Paris today?"s;
    REQUIRE(engine.recognize_intent(input) == "Intent: Get Weather City");
  }

  SECTION("Check fact intention") {
    std::string input = "Tell me an interesting fact."s;
    REQUIRE(engine.recognize_intent(input) == "Intent: Get Fact");
  }

  SECTION("Check fact intention") {
    std::string input = "Tell me an interesting fact. No no no no no"s;
    REQUIRE(engine.recognize_intent(input).has_value() == false);
  }
}
