#include <catch2/catch_test_macros.hpp>

#include "preprocessor.h"

namespace eir = embedded::intent::recognizer;

TEST_CASE("Extract words") {
  std::string input = "Hello! Can I order the coffee?";
  auto output = eir::process_input(input);
  REQUIRE(output.size() == 4);
  REQUIRE(output[0] == "hello");
  REQUIRE(output[3] == "coffee");
}

TEST_CASE("Extract words from intention") {
  std::string input = "What is the weather like today?";
  auto output = eir::process_input(input);
  REQUIRE(output.size() == 3);
  REQUIRE(output[0] == "weather");
  REQUIRE(output[2] == "today");
}

TEST_CASE("Extract one word") {
  std::string input = "undefined";
  auto output = eir::process_input(input);
  REQUIRE(output.size() == 1);
  REQUIRE(output[0] == "undefined");
}
