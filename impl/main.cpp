#include "embedded/intent/recognizer/interpreter_factory.h"

namespace eir = embedded::intent::recognizer;

int main()
{
  auto interpreter = eir::make_interpreter(eir::EngineType::BayesianEngine);
  interpreter->run();
}
