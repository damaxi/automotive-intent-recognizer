#include "bayesian_rule.h"

namespace embedded::intent::recognizer
{

namespace
{

auto const zero_coeff = 0.1;

}

BayesianRule::BayesianRule(std::string const& name,
                           std::vector<Word>&& words,
                           RuleCallback callback)
  : m_name(name), m_words(words), m_callback(callback)
{}

double BayesianRule::get_word_probability(std::string_view const& str) const
{
  auto it = std::find_if(std::begin(m_words), std::end(m_words),
                         [&str](auto const& w) {
                           return w.word == str;
                         });
  if (it == std::end(m_words)) return zero_coeff;
  return it->probability;
}

BayesianRule::operator RuleResult() const
{
  RuleResult result{m_name, m_callback, 0.0};
  return result;
}

RuleResults
analyze_preprocessed_output_by_rules(PreprocessedOutput const& output,
                                     BayesianRules const& rules)
{
  RuleResults results;
  for (auto const& rule : rules) {
    double score{1};
    for (auto const& o : output) {
      score *= rule.get_word_probability(o);
    }
    RuleResult result = rule;
    result.score = score;
    results.emplace_back(result);
  }
  return results;
}

bool is_result_acceptable(RuleResult const& result)
{
  return result.score > zero_coeff;
}

}
