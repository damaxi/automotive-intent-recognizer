#include "bayesian_engine.h"

#include <algorithm>

#include "embedded_rules.h"
#include "preprocessor.h"

namespace embedded::intent::recognizer
{

namespace
{

RuleResult select_result(RuleResults const& results)
{
  auto it = std::max_element(results.begin(), results.end(),
                   [](auto const& a, auto const& b){ return a < b; });
  return *it;
}

}

BayesianEngine::BayesianEngine()
  : m_rules(create_rules())
{}

std::optional<std::string>
BayesianEngine::recognize_intent(std::string& input) const
{
  if (input.empty()) return {};

  auto const& output = process_input(input);
  auto const& results = analyze_preprocessed_output_by_rules(output, m_rules);
  auto const& best_result = select_result(results);
  if (is_result_acceptable(best_result)) [[likely]]
    return best_result.callback(output);

  return {};
}

}
