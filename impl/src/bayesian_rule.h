#ifndef BAYESIAN_RULE_H
#define BAYESIAN_RULE_H

#include <vector>
#include <string>
#include <functional>

#include "rule.h"
#include "preprocessor.h"

namespace embedded::intent::recognizer
{

class BayesianRule final
{
public:
  struct Word
  {
    std::string word;
    double probability;
  };

  BayesianRule(std::string const& name,
               std::vector<Word>&& words,
               RuleCallback callback);

  operator RuleResult() const;

  [[nodiscard]] double
  get_word_probability(std::string_view const& str) const;

private:
  std::string m_name;
  std::vector<Word> m_words;
  RuleCallback m_callback;
};

using BayesianRules = std::vector<BayesianRule>;

RuleResults
analyze_preprocessed_output_by_rules(PreprocessedOutput const& output,
                                     BayesianRules const& rules);

bool is_result_acceptable(RuleResult const& result);

}

#endif
