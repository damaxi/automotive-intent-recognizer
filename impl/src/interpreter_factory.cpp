#include "embedded/intent/recognizer/interpreter_factory.h"

#include "bayesian_engine.h"

namespace embedded::intent::recognizer
{

std::unique_ptr<Interpreter> make_interpreter(EngineType etype)
{
  switch (etype) {
  case EngineType::BayesianEngine:
    auto engine = std::make_unique<BayesianEngine>();
    return std::make_unique<Interpreter>(std::move(engine));
  }
}

}
