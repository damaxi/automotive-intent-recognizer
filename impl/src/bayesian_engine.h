#ifndef BAYESIANENGINE_H
#define BAYESIANENGINE_H

#include "embedded/intent/recognizer/engine.h"
#include "bayesian_rule.h"

namespace embedded::intent::recognizer
{

class BayesianEngine final : public Engine
{
public:
  BayesianEngine();

  BayesianEngine(BayesianEngine const&) = delete;

  BayesianEngine& operator=(BayesianEngine const&) = delete;

  std::optional<std::string>
  recognize_intent(std::string& input) const override;

private:
  const BayesianRules m_rules;
};

}

#endif
