#ifndef RULE_H
#define RULE_H

#include <functional>

#include "preprocessor.h"

namespace embedded::intent::recognizer
{

  using RuleCallback = std::function<std::string(PreprocessedOutput const&)>;

struct RuleResult
{
  std::string_view name;
  RuleCallback callback;
  double score;
};

inline bool operator<(RuleResult const& a, RuleResult const& b)
{
  return a.score < b.score;
}

using RuleResults = std::vector<RuleResult>;

}

#endif
