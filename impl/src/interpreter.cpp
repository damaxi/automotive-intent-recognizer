#include "embedded/intent/recognizer/interpreter.h"

#include <cstdio>
#include <iostream>

namespace embedded::intent::recognizer
{

namespace
{

static constexpr auto s_help_message = R"(
Welcome to intent recognizetion interpreter!
Write your intention and press <ENTER>
    The following options are available:
    exit - exits REPL loop or press Ctrl+<D>
)";

void help()
{
  std::printf(s_help_message);
}

}

Interpreter::Interpreter(std::unique_ptr<Engine>&& engine)
  : m_engine(std::move(engine))
{}

void Interpreter::run() const
{
  std::string text;
  help();
  std::cout << ">>> ";
  while (std::getline(std::cin, text)) {
    if (auto const intent = m_engine->recognize_intent(text)) {
      std::cout << *intent << std::endl;
    }
    else {
      std::cout << "Intent: Uncategorized intent" << std::endl;
    }
      std::cout << ">>> ";
  }
}

}
