#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H

#include <vector>
#include <string>
#include <string_view>

namespace embedded::intent::recognizer
{

using PreprocessedOutput = std::vector<std::string_view>;

PreprocessedOutput process_input(std::string& input);

}

#endif
