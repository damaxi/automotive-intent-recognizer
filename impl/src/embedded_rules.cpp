#include "embedded_rules.h"

namespace embedded::intent::recognizer
{

namespace
{

std::string get_weather_clbk([[maybe_unused]] PreprocessedOutput const& output)
{
  return "Intent: Get Weather";
}
BayesianRule get_weather = {"GetWeather",
                           {{"weather", 90.0}, {"today", 10.0}},
                           get_weather_clbk};

std::string get_weather_city_clbk([[maybe_unused]] PreprocessedOutput const& output)
{
  return "Intent: Get Weather City";
}
BayesianRule get_weather_city = {"GetWeatherCity",
                                {{"weather", 90.0}, {"today", 10.0},
                                {"in", 90.0}}, get_weather_city_clbk};

std::string get_fact_clbk([[maybe_unused]] PreprocessedOutput const& output)
{
  return "Intent: Get Fact";
}
BayesianRule get_fact = {"GetFact",
                        {{"interesting", 10.0}, {"fact", 80.0}},
                        get_fact_clbk};
}

BayesianRules create_rules()
{
  return {get_weather, get_weather_city, get_fact};
}

}
