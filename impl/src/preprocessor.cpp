#include "preprocessor.h"

#include <algorithm>
#include <array>
#include <cctype>

namespace embedded::intent::recognizer
{

namespace
{

void to_lower(std::string& input)
{
  for (auto& l : input) {
    auto c = static_cast<unsigned char>(l);
    if (std::isalpha(c))
      l = std::tolower(c);
  }
}

constexpr std::array<char const*, 10> filtered_set =
{
  "the", "a", "an", "i", "why", "what", "is", "are", "tell", "me"
};

bool should_exclude(std::string_view word)
{
  return std::any_of(filtered_set.cbegin(), filtered_set.cend(),
                     [&word](char const* f) { return word == f; });
}

}

std::vector<std::string_view> process_input(std::string& input)
{
  to_lower(input);
  std::vector<std::string_view> ret;
  bool has_word = false;
  std::size_t saved_i = 0;
  for (std::size_t i = 0; i < input.length(); ++i) {
    auto is_letter = std::isalpha(static_cast<unsigned char>(input[i]));
    if (!has_word && is_letter) {
      has_word = true;
      saved_i = i;
    }
    else if (has_word && !is_letter) {
      has_word = false;
      std::string_view const word{input.c_str() + saved_i,
                                  static_cast<std::size_t>(i - saved_i)};
      if (not should_exclude(word))
        ret.emplace_back(word);
    }
  }

  if (has_word) {
    std::string_view const word{input.c_str() + saved_i,
                                static_cast<std::size_t>(input.length()
                                                         - saved_i)};
    ret.emplace_back(word);
  }
  return ret;
}

}
