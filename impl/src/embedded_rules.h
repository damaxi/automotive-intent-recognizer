#ifndef EMBEDDED_RULES_H
#define EMBEDDED_RULES_H

#include "bayesian_rule.h"

namespace embedded::intent::recognizer
{

BayesianRules create_rules();

}

#endif
