#ifndef ENGINE_H
#define ENGINE_H

#include <optional>
#include <string>

namespace embedded::intent::recognizer
{

class Engine
{
public:
  virtual std::optional<std::string>
  recognize_intent(std::string& input) const = 0;

  Engine() = default;

  Engine(Engine const&) = delete;

  Engine& operator=(Engine const&) = delete;

  virtual ~Engine() = default;
};

}

#endif
