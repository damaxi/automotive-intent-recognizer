#ifndef INTERPRETER_FACTORY_H
#define INTERPRETER_FACTORY_H

#include "embedded/intent/recognizer/interpreter.h"

#include <cstdint>
#include <memory>

namespace embedded::intent::recognizer
{

enum class EngineType : std::uint8_t
{
  BayesianEngine,
};

std::unique_ptr<Interpreter> make_interpreter(EngineType etype);

}

#endif
