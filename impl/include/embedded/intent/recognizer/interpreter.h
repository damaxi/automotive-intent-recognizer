#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "embedded/intent/recognizer/engine.h"

namespace embedded::intent::recognizer
{

class Interpreter final
{
public:
  Interpreter(std::unique_ptr<Engine>&& engine);

   void run() const;

private:
  std::unique_ptr<Engine> m_engine;
};

}

#endif
