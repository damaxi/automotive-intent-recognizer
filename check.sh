#!/usr/bin/env bash

mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
find impl/ -type f -name "*.cpp" -o -name "*.h" | xargs clang-tidy13 -p build
valgrind --tool=memcheck --leak-check=yes test/intent-recognizer_test
